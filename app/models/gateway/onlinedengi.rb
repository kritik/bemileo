class Gateway:: Onlinedengi < Gateway
  preference :project, :string
  preference :secret, :string


  def provider_class
    self.class
  end

  def method_type
    "onlinedengi"
  end

  def test?
    options[:test_mode] == true
  end

  def url
    self.test? ? "http://google.com" : "yandex.ru"
  end

  def self.current
    self.where(:type => self.to_s, :environment => Rails.env, :active => true).first
  end

  def desc
    "<p>
      <label> #{I18n.t('robokassa.success_url')}: </label> http://[domain]/onlinedengi/success<br />
      <label> #{I18n.t('robokassa.result_url')}: </label> http://[domain]/onlinedengi/result<br />
      <label> #{I18n.t('robokassa.fail_url')}: </label> http://[domain]/onlinedengi/fail<br />
    </p>"
  end
end
