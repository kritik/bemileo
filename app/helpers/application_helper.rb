module ApplicationHelper
  # to create menu in the header
  def special_menu_creator
    links = {:homepage => root_path,
             :whats_new => news_index_path,
             :specials => "#",
             :contact_us => contact_index_path
    }
    if session['warden.user.user.key'].nil?
      links["login"] = login_path
    else
      links["logout"] = {:action=>"destroy", :controller=>"/user_sessions"}
    end
    i = 0
    links.collect do |name,to_go|
      request
      "<td id=\"#{"over_" if request.url.eql?(to_go)}m#{i+=1}\" onMouseOut=\"this.id='#{"over_" if request.url.eql?(to_go)}m#{i}';\" onMouseOver=\"this.id='over_m#{i}';\">
          #{link_to t(name), to_go}
		  </td>"
    end.join("<td class=\"menu_separator\"><img src=\"/images/menu_separator.gif\" border=\"0\" alt=\"\" width=\"1\" height=\"13\"></td>").html_safe
  end

  def header_menu_creator links, count_icons = false, domain = ""
    i=0
    links.collect do |name,to_go|
      i+=1
      "<li #{"class=\"icon_#{i}\" " if count_icons}>#{link_to t(name),  "#{request.protocol}#{request.domain}#{to_go}"}</li>"
    end.join('').html_safe
  end

  #loads locales for language module
  def get_locales
    fall_back = [:ru].to_yaml
    locales = CustomSetting.find_or_create_by_name("languages").value || fall_back
    begin
      locales = YAML::load(locales)
    rescue
      locales = YAML::load(fall_back)
    end
  end

  # human readable list of variant options
  def variant_options(v, allow_back_orders = Spree::Config[:allow_backorders], include_style = true)
    list = v.options_text
    list = include_style ? "<span class =\"out-of-stock\">(" + t("out_of_stock") + ") #{list}</span>" : "#{t("out_of_stock")} #{list}" unless (allow_back_orders || v.in_stock?)
    list
  end
  
  # human readable list of variant options
  # should be cut as dropbox variants should be in use
  def variant_options2(v, allow_back_orders = Spree::Config[:allow_backorders], include_style = true)
    #list = v.options_text
    #list = include_style ? content_tag(:span, "(#{t(:out_of_stock)}) #{list}#{v.id}", :class => "out-of-stock") : "#{t(:out_of_stock)} #{list}" unless (allow_back_orders || v.in_stock?)
    #list
    option_values = v.option_values
    table = "<table>"
    option_values.each do |option_value|
      table << "<tr><td>#{option_value.option_type.name}</td><td>#{option_value.name}</td></tr>"
    end
    table << "<tr><td>#{t(:out_of_stock)}</td><td>#{t(:order)}</td></tr>" unless (allow_back_orders || v.in_stock?)
    table << "</table>"
    table.html_safe
  end

  def per_page_links(variants, current)
    variants.to_a.map do |v|
      css_class =  current.eql?(v) ? 'current' : ''
      "<li>#{link_to v, params.merge({:per_page => v}), :class=> css_class }</li>"
    end.join.html_safe
  end

  def sort_links(variants, current)
    variants.to_a.map do |k,v|
      css_class =  current.to_sym.eql?(k.to_sym) ? 'current' : '' if current
      "<li>#{link_to k, params.merge({:sort => v}), :class=> css_class }</li>"
    end.join.html_safe
  end
end
