class Gateway::OnlinedengiController < Spree::BaseController
  skip_before_filter :verify_authenticity_token, :only => [:result, :success, :fail]
  before_filter :parse_payment_params, :only => [:result]
  before_filter :valid_payment, :only => [:result]
  before_filter :my_debuger

  def show
    @order = Order.find(params[:order_id])
    @gateway = @order.available_payment_methods.find { |x| x.id == params[:gateway_id].to_i }
    @order.payments.destroy_all
    payment = @order.payments.create!(:amount => 0, :payment_method_id => @gateway.id)
    @secure_text = Page.find_by_slug('/secure').try(:body)

    if @order.blank? || @gateway.blank?
      flash[:error] = I18n.t("Invalid arguments")
      redirect_to :back
    else
      render :action => :show
    end
  end

  def result
    raise GatewayError, "Not found order" unless @order
    payment = @order.payments.first
    payment.state = "completed"
    payment.amount = @payment_params[:payment_amount].to_f
    payment.save
    @order.save!
    @order.next! until @order.state == "complete"
    render :text => SUCCESS_ANSWER
  end

  def success
    @order = Order.find_by_id(session[:order_id])
    if @order && @order.complete?
      session[:order_id] = nil
      redirect_to order_path(@order)
    else
      flash[:error] = t("payment_fail")
      redirect_to root_url
    end
  end

  def fail
    @order = Order.find_by_id(session[:order_id])
    flash[:error] = params[:err_msg].first.force_encoding('windows-1251').encode('utf-8') if params[:err_msg].try(:first) #t("payment_fail")
    redirect_to @order.blank? ? root_url : checkout_state_url(:state => "payment")
    return
  end

  private

  RESULT_HEADERS = ['amount', 'userid', 'userid_extra', 'paymentid', 'key', 'paymode', 'orderid', 'serverid']
  SUCCESS_ANSWER="<result><id></id><code>YES</code><comment></comment><course></course></result>"
  FAIL_ANSWER="<result><id></id><code>NO</code><comment></comment><course></course></result>"
  # parse params
  def parse_payment_params
    @payment_params = {}
    params.each { |key, value| @payment_params[key.intern] = value if RESULT_HEADERS.include? key }
  end
end


def valid_payment
  Rails.logger.info "Request resolved: #{@payment_params.inspect}"
  puts "Request resolved: #{@payment_params.inspect}"
  @order = Order.find_by_id(@payment_params[:orderid])
  @gateway = @order.payments.first.payment_method

  raise "invalid gateway" unless @gateway
  @secret = @gateway.options[:secret]

  if @secret.blank? # если не указан секретный ключ
    raise ArgumentError.new("onlinedengi secret key is not provided")
  elsif !valid_hash?(@payment_params, @secret)
    raise "not valid payment - secret not correct"
  end
rescue => ex
  Rails.logger.info "not valid payment #{ex.message}"
  puts "not valid payment #{ex.message}"
  render :text => SUCCESS_ANSWER
end


def valid_hash?(payment_params, secret)
  Rails.logger.info payment_params[:key]
  Rails.logger.info secret
  Rails.logger.info [payment_params[:amount], payment_params[:userid], payment_params[:paymentid], secret].join('')
  puts payment_params[:key]
  puts secret
  puts [payment_params[:amount], payment_params[:userid], payment_params[:paymentid], secret].join('')
  puts Digest::MD5.hexdigest([payment_params[:amount], payment_params[:userid], payment_params[:paymentid], secret].join(''))
  payment_params[:key] == Digest::MD5.hexdigest([payment_params[:amount], payment_params[:userid], payment_params[:paymentid], secret].join(''))
end

def my_debuger
  p "my debugger"
  p "my debugger"
  p "my debugger"
  p "my debugger"
  p "my debugger"
  p "my debugger"
  p params
end