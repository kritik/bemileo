CheckoutController.class_eval do
#  before_filter :copy_addresses, :only => :update
  before_filter :redirect_to_onlinedengi_form_if_needed, :only => :update

  private
  def remove_payments_attributes_if_total_is_zero
    return unless params[:order] && params[:order][:store_credit_amount]
  end
  
  def copy_addresses
    return unless params[:state] == "address"
    
    Rails.logger.error "pre "+params[:order].inspect
    params[:order][:ship_address_attributes] = params[:order][:bill_address_attributes] if params[:order][:bill_address_attributes][:id].nil?
    unless current_order.update_attributes(params[:order])
      flash[:error] = current_order.errors[:base]
      respond_with(@order, :location => :address) && return
    end
  end

  # Redirect to onlinedengi
  #
  def redirect_to_onlinedengi_form_if_needed
    return unless params[:state] == "payment"#params[:state] == "address"
    return unless params[:order][:payments_attributes]
    payment_method = PaymentMethod.find(params[:order][:payments_attributes].first[:payment_method_id])
    if payment_method.kind_of? Gateway::Onlinedengi
      if !request.ssl? && Rails.env =~ /production/
        redirect_to onlinedengi_path(:gateway_id => payment_method.id, :order_id => @order.id,
                                          :protocol => "https")
      else
        redirect_to onlinedengi_path(:gateway_id => payment_method.id, :order_id => @order.id)
      end
    end

  end

end
