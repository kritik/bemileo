# encoding: UTF-8
=begin
Order.class_eval do
  Order.state_machines[:state] = StateMachine::Machine.new(Order, :initial => 'cart', :use_transactions => false) do
    event :next do
         transition :from => 'cart', :to => 'address'
         transition :from => 'address', :to => 'complete'
         #transition :from => 'delivery', :to => 'complete'
         #transition :from => 'address', :to => 'complete'
         #transition :from => 'confirm', :to => 'complete'
         # note: some payment methods will not support a confirm step
         #transition :from => 'payment', :to => 'confirm',
         #           :if => Proc.new { Gateway.current && Gateway.current.payment_profiles_supported? }
         #transition :from => 'payment', :to => 'complete'
       end

    event :cancel do
      transition :to => 'canceled', :if => :allow_cancel?
    end
    event :return do
      transition :to => 'returned', :from => 'awaiting_return'
    end
    event :resume do
      transition :to => 'resumed', :from => 'canceled', :if => :allow_resume?
    end
    event :authorize_return do
      transition :to => 'awaiting_return'
    end

    before_transition :to => 'complete' do |order|
      begin
        order.process_payments!
      rescue Spree::GatewayError
        if Spree::Config[:allow_checkout_on_gateway_error]
          true
        else
          false
        end
      end
    end

    before_transition :from => 'address' do |order|
      #Rails.logger.error "HOOKED!"
      #Rails.logger.error order.ship_address.inspect
      ##Rails.logger.error order.ship_address_attributes
      #Rails.logger.error order.inspect
      #Rails.logger.error order.shipping_method
      order.shipping_method = ShippingMethod.all_available(order).first
      #Rails.logger.error order.shipping_method

    end

    after_transition :to => 'complete', :do => :finalize!
    after_transition :to => 'delivery', :do => :create_tax_charge!
    after_transition :to => 'payment', :do => :create_shipment!
    after_transition :to => 'canceled', :do => :after_cancel
  end

end
=end
