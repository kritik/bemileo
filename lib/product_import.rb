module ProductImport
  require 'csv'

  def upload_products file
    @file = file
    I18n.locale = :en

      # TODO: put default values
      p "TODO: put default values"
      p "TODO: uncomment taxon's search"
      tax_category_id = nil
      shipping_category_id = nil
      upload_path =  File.join(Rails.root, 'public','upload')

      require 'csv'
      CSV.parse(@file.read(),  {:headers => :first_row, :col_sep => ","}).each do |row|
        I18n.locale = :en
        if row["id"]
          product = Product.find(row["id"])
        else
          variant = Variant.find_by_sku(row["sku"])
          if variant
            product = variant.product
            product = variant.create_product unless product
          else
            product = Product.new
          end
        end

        p row["name"].force_encoding("utf-8")
        product.name = row["name"].force_encoding("utf-8")
        product.description = row["description"].force_encoding("utf-8")
        product.sku = row["sku"].force_encoding("utf-8")


        #if no variant yet, we should set master price to product
        #unless variant
          p row["price"]
          product.price = row["price"].to_s.gsub(",","").to_f
          product.cost_price = row["cost_price"].to_s.gsub(",","").to_f
          product.count_on_hand = row["count_on_hand"]
          product.available_on = Time.now
          product.tax_category_id = tax_category_id
          product.shipping_category_id = shipping_category_id
          product.meta_description = row["meta_description"].force_encoding("utf-8")
        #end
        product.save!

        # creating options and values
        options = row['options'].split(',').map{|el| el.strip}
        option_names = row['option_names'].split(',').map{|el| el.strip}
        variant_values = []
        puts "Options #{options.size}"
        puts "Option names #{option_names.size}"
        next unless options.size.eql?(option_names.size)

        option_names.each_with_index do |option_name, index|
            option = OptionType.find_by_name option_name
            unless option
              option = OptionType.new(:name => option_name, :presentation=>option_name)
              option.save!
            end

            if option_value = option.option_values.select{|option_value| option_value.name.eql?(options[index].force_encoding("utf-8"))}.first
              variant_values << option_value
            else
              option_value = option.option_values.create(:name => options[index], :presentation => options[index])
              option_value.save!
              variant_values << option_value
            end
            product.option_types += [option] unless product.option_type_ids.include?(option.id)
        end
        product.save!

        #creating variants
        if variant = product.variants.select{|variant| variant.barcode.eql?(row["barcode"].force_encoding("utf-8"))}.first
          variant
        else
          variant = product.variants.create(:barcode => row["barcode"].force_encoding("utf-8"))
          variant.barcode = row["barcode"].force_encoding("utf-8")
        end
        variant.sku = row["sku"].force_encoding("utf-8")
        variant.price = row["price"].to_s.gsub(",","").to_f
        variant.cost_price = row["cost_price"].to_s.gsub(",","").to_f
        # check original file for this data
        variant.special_price = row["special_price"].blank? ? nil : row["special_price"].to_s.gsub(",","").to_f
        variant.count_on_hand = row["count_on_hand"]
        variant.option_values = variant_values
        p "Variant not saved" unless variant.save!
        product.save!

        #adding taxons to product
        row['taxonomy'] = row['taxonomy'].force_encoding("utf-8")
        next unless row['taxons']
        row['taxons']   = row['taxons'].force_encoding("utf-8")
        if taxonomy = Taxonomy.find_by_name(row['taxonomy'])
	        taxonomy = taxonomy.first if taxonomy.is_a?(Array)
        else
	        taxonomy = Taxonomy.new(:name=>row['taxonomy'])
	        taxonomy.save!
        end
        row['taxons'].split(',').each do |taxon|
            #checking
            o_taxon = Taxon.where(:name => taxon.strip, :taxonomy_id => taxonomy.id)#, :parent_id => taxonomy.taxons.first.id)
            o_taxon = o_taxon.first
            unless o_taxon
              o_taxon = Taxon.new(:name=>taxon.strip, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.taxons.first.id)
              o_taxon.save!
            end
            if o_taxon
              o_taxon.parent_id = taxonomy.taxons.first.id
              o_taxon.save
            end
            product.taxons += [o_taxon] unless product.taxon_ids.include?(o_taxon.id)
        end

        #adding the pictures
        img_dir=File.join(upload_path, 'photos',product.sku.gsub(/ /,""))
        if Dir.exists?(img_dir)
          pics = Dir.open(img_dir).map{|d| d} - ['.','..']
          pics.each do |pic|
            file = File.open(File.join(img_dir,pic),'rb')
            product_image = Image.new({:attachment => file,
                                :viewable => product,
                                :position => product.images.length
                                })
            product.images << product_image if product_image.save
            product.save!
          end
        end

        #translations
        I18n.locale = :ru
        product.name = row["name"].force_encoding("utf-8")
        product.description = row["description"].force_encoding("utf-8")
        product.save!

        puts "Importing of product #{product.name} with sku '#{product.sku}' was finished"
    end
    #
    #CSV.parse(@file.read(),  {:headers => :first_row, :col_sep => ","}).each do |row|
    #  read_parameters row
    #  upload_product
    #end
  end

  def upload_product
    begin
      puts "Import product: sku - '#{@sku}'"

      @product = find_or_create_product
      update_product_data
      load_variant create_options
      set_taxons
      load_pictures

      @product.save!

      puts "Importing of product with sku '#{@product.sku}' was finished"
    rescue
      puts "Error at uploading product"
    end
  end

  def read_parameters row
    I18n.locale = :en
    @id = row["id"]
    @sku = row["sku"].force_encoding("utf-8")
    @price = row["price"].to_s.gsub(",","").to_f
    @cost_price = row["cost_price"].to_s.gsub(",","").to_f
    @count_on_hand = row["count_on_hand"]
    @meta_description = row["meta_description"].force_encoding("utf-8")
    @options = row['options'].split(',').map{|el| el.strip}
    @option_names = row['option_names'].split(',').map{|el| el.strip}
    @barcode = row["barcode"].force_encoding("utf-8")
    @special_price = row["special_price"].blank? ? nil : row["special_price"].to_s.gsub(",","").to_f
    @taxonomy = row['taxonomy'].force_encoding("utf-8")
    @taxons = row['taxons'].force_encoding("utf-8")

    #I18n.locale = :ru
    @name = row["name"].force_encoding("utf-8")
    @description = row["description"].force_encoding("utf-8")
    I18n.locale = :en
  end

  def find_or_create_product
    if @id
      product = Product.find(@id)
    else
      variant = Variant.find_by_sku(@sku)
      if variant
        product = variant.product
        product = variant.create_product unless product
      else
        product = Product.new
      end
    end
    product
  end

  def update_product_data
    @product.name = @name
    @product.description = @description
    @product.sku = @sku
    @product.price = @price
    @product.cost_price = @cost_price
    @product.count_on_hand = @count_on_hand
    @product.available_on = Time.now
    @product.meta_description = @meta_description
    I18n.locale = :ru
    @product.save!
  end

  def create_options
    puts "Create options and values"
    variant_values = []

    if @options.size.eql?(@option_names.size)
      puts "Error with creating options for product #{@sku}: options size - #{options.size}, option names size- #{option_names.size}"
      yield variant_values = []
    end

    @option_names.each_with_index do |option_name, index|
        option = OptionType.find_by_name option_name
        unless option
          option = OptionType.new(:name => option_name, :presentation=>option_name)
          option.save!
        end

        option_value = option.option_values.select{|option_value| option_value.name.eql?(options[index].force_encoding("utf-8"))}.first
        unless option_value
          option_value = option.option_values.create(:name => options[index], :presentation => options[index])
          option_value.save!
        end
        variant_values << option_value

        @product.option_types += [option] unless product.option_type_ids.include?(option.id)
    end

    variant_values
  end

  def load_variant variant_values
    variant = @product.variants.select{|variant| variant.barcode.eql?(@barcode)}.first
    unless variant
      variant = @product.variants.create(:barcode => @barcode)
      variant.barcode = @barcode
    end
    variant.sku = @sku
    variant.price = @price
    variant.cost_price = @cost_price
    variant.special_price = @special_price
    variant.count_on_hand = @count_on_hand
    variant.option_values = variant_values

    p "Variant not saved" unless variant.save!
  end

  def set_taxons
    unless @taxons
      if taxonomy = Taxonomy.find_by_name(@taxonomy)
        taxonomy = taxonomy.first if taxonomy.is_a?(Array)
      else
        taxonomy = Taxonomy.new(:name=>@taxonomy)
        taxonomy.save!
      end

      @taxons.each do |taxon|
        o_taxon = Taxon.where(:name => taxon.strip, :taxonomy_id => taxonomy.id)#, :parent_id => taxonomy.taxons.first.id)
        o_taxon = o_taxon.first
        unless o_taxon
          o_taxon = Taxon.new(:name=>taxon.strip, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.taxons.first.id)
          o_taxon.save!
        end
        if o_taxon
          o_taxon.parent_id = taxonomy.taxons.first.id
          o_taxon.save
        end
        @product.taxons += [o_taxon] unless @product.taxon_ids.include?(o_taxon.id)
      end
    end
  end

  def load_pictures
    upload_path =  File.join(Rails.root, 'public','upload')
    img_dir=File.join(upload_path, 'photos',@product.sku.gsub(/ /,""))
    if Dir.exists?(img_dir)
      pics = Dir.open(img_dir).map{|d| d} - ['.','..']
      pics.each do |pic|
        file = File.open(File.join(img_dir,pic),'rb')
        product_image = Image.new({:attachment => file,
                            :viewable => @product,
                            :position => @product.images.length
                            })
        @product.images << product_image if product_image.save
      end
    end
  end

end