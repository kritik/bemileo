class AddPositionToTaxonomy < ActiveRecord::Migration
  def self.up
    add_column :taxonomies, :position, :integer
  end

  def self.down
    remove_column :taxonomies, :position
  end
end
