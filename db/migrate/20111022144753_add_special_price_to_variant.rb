class AddSpecialPriceToVariant < ActiveRecord::Migration
  def self.up
    add_column :variants, :special_price, 'decimal(8,2)'
  end

  def self.down
    remove_column :variants, :special_price
  end
end
