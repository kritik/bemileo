class AddGroupingToProductGroups < ActiveRecord::Migration

  def self.up
    add_column :product_groups, :grouping, :string
  end

  def self.down
    remove_column :product_groups, :grouping
  end
end
