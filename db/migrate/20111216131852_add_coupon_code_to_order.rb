class AddCouponCodeToOrder < ActiveRecord::Migration
  def self.up
    add_column :orders, :my_coupon_code, :string
  end

  def self.down
    remove_column :orders, :my_coupon_code
  end
end
