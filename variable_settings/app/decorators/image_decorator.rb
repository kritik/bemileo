Image.class_eval do
  attachment_definitions[:attachment][:styles] = {
  :mini => '102x102>',
#   :small => '160x160',
  :small => '195x195>',
  :product => '218x218>',
  :large => '319x275>',
  :full_size => '1000x768>'
}
end