TaxonsController.class_eval do
  def show
    @taxon = Taxon.find_by_permalink!(params[:id])
    return unless @taxon

    
    if params.key?(:keywords)
      @searcher = Spree::Config.searcher_class.new(params.merge(:taxon => @taxon.id))
      @products = @searcher.retrieve_products
    else
      filters = params[:search] ? params[:search][:group_filter] : []
      @products = @taxon.products_by_groups(filters)
    end

    if session[:sort]    
      case session[:sort].to_sym
      when :price
	@products.sort_by! &:price
      when :newest
	@products.sort_by! &:created_at
      when :name
	@products.sort_by! &:name
      end
    end
    
    @products = @products.paginate(
                  :page => params[:page], 
                  :per_page => session[:per_page]
                  )
                  
    respond_with(@taxon)
  end
end
