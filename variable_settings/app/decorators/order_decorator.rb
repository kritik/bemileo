Order.class_eval do
  attr_accessible :my_coupon_code
  
  def add_variant(variant, quantity = 1)
    current_item = contains?(variant)
    if current_item
      current_item.quantity += quantity
      current_item.save
    else
      current_item = LineItem.new(:quantity => quantity)
      current_item.variant = variant
      current_item.price   = variant.price
      current_item.price   = variant.special_price if variant.special_price && !variant.special_price.blank?
      self.line_items << current_item
    end

    # populate line_items attributes for additional_fields entries
    # that have populate => [:line_item]
    Variant.additional_fields.select{|f| !f[:populate].nil? && f[:populate].include?(:line_item) }.each do |field|
      value = ""

      if field[:only].nil? || field[:only].include?(:variant)
        value = variant.send(field[:name].gsub(" ", "_").downcase)
      elsif field[:only].include?(:product)
        value = variant.product.send(field[:name].gsub(" ", "_").downcase)
      end
      current_item.update_attribute(field[:name].gsub(" ", "_").downcase, value)
    end

    current_item
  end
  
  
  # Credits
  def max_credit_to_use
    begin
      constraint = YAML::load( CustomSetting.find_or_create_by_name("credit").value || {})
    rescue
      constraint = {}
    end
    constraint[:max_charging] ||= 0
    constraint[:max_charging] = constraint[:max_charging]/100 if constraint[:max_charging].to_f > 1
    #return [user.store_credits_total, (item_total * constraint[:max_charging])].min
    return [user.store_credits_total* constraint[:max_charging], (item_total)].min
  end
  
  def consume_users_credit
    return unless completed?
    credit_used = self.store_credit_amount

    user.store_credits.each do |store_credit|
      break if credit_used == 0
      if store_credit.remaining_amount > 0
        if store_credit.remaining_amount > credit_used
          store_credit.remaining_amount -= credit_used
          store_credit.save
          credit_used = 0
        else
          credit_used -= store_credit.remaining_amount
          store_credit.update_attribute(:remaining_amount, 0)
        end
      end
    end

    finalize!
  end
  
  def process_store_credit
    @store_credit_amount = BigDecimal.new(@store_credit_amount.to_s.gsub(/ /,'').to_s).round(2)

    # store credit can't be greater than order total (not including existing credit), or the users available credit
    
    @store_credit_amount = [@store_credit_amount, max_credit_to_use].flatten.min

    if @store_credit_amount <= 0
      if sca = adjustments.detect {|adjustment| adjustment.source_type == "StoreCredit" }
        sca.destroy
      end
    else
      if sca = adjustments.detect {|adjustment| adjustment.source_type == "StoreCredit" }
        sca.update_attributes({:amount => -(@store_credit_amount)})
      else
        #create adjustment off association to prevent reload
        sca = adjustments.create(:source_type => "StoreCredit",  :label => I18n.t(:store_credit) , :amount => -(@store_credit_amount))
      end

      #recalc totals and ensure payment is set to new amount
      update_totals
      payment.amount = total if payment
    end
  end
end
