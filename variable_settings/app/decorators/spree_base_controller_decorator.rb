Spree::BaseController.class_eval do
  before_filter :load_paginations
  
  protected
  def load_paginations
    session[:per_page] = params[:per_page] unless params[:per_page].blank?
    session[:per_page] = 30 if session[:per_page].blank?
    
    session[:sort] = params[:sort].to_sym unless params[:sort].blank?
    params[:sort] = session[:sort]
    #session[:sort] = :price if session[:sort].blank? 
  end
end