Product.class_eval do  
  ajaxful_rateable :stars => 5

  delegate_belongs_to :master, :special_price if Variant.table_exists? && Variant.column_names.include?("special_price")
  
  #from solr
  def size_option
    get_option_values('product-size')
  end
  
  def gem_type_property
    pp = ProductProperty.first(:joins => :property,
          :conditions => {:product_id => self.id, :properties => {:name => 'gem-type'}})
    pp ? pp.value : ''
  end
  
  def product_weight
    pp = ProductProperty.first(:joins => :property,
          :conditions => {:product_id => self.id, :properties => {:name => 'product-weight'}})
    pp ? pp.value : ''
  end
end

#ProductsController.class_eval do
#def rate
#    @product = Product.find(params[:id])
#    @product.rate(params[:stars], current_user, params[:dimension])
#    average = @product.rate_average(true, params[:dimension])
#    width = (average / @product.class.max_stars.to_f) * 100
#    render :json => {:id => @product.wrapper_dom_id(params), :average => average, :width => width}
#  end
#end