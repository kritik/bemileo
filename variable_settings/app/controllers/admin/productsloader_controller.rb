class Admin::ProductsloaderController < Admin::BaseController
  resource_controller

  require 'csv'

  def load_products
    @file = params[:file]
    render 'load_products'
  end
    require 'csv'
  def upload
    begin
      @file = params[:file]
      task
      render :text => "products uploaded"
    rescue
      render :text => "Error while uploading products"
     end
  end
     require 'csv'
  def task
      I18n.locale = :en
      # TODO: put default values
      p "TODO: put default values"
      p "TODO: uncomment taxon's search"
      tax_category_id = nil
      dir_images_skus_done = []
      shipping_category_id = nil
      upload_path =  File.join(Rails.root, 'public','upload')
        require 'csv'
      CSV.parse(@file.read(),  {:headers => :first_row, :col_sep => ";"}).each do |row|
        I18n.locale = :en
        if row["id"]
          product = Product.find(row["id"])
        else
          variant = Variant.find_by_sku(row["sku"])
          if variant
            product = variant.product
            product = variant.create_product unless product
          else
            product = Product.new
          end
        end

        p row["name"].force_encoding("utf-8")
        product.name = row["name"].force_encoding("utf-8")
        product.description = row["description"].force_encoding("utf-8")
        product.sku = row["sku"].force_encoding("utf-8")


        #if no variant yet, we should set master price to product
        #unless variant
          p row["price"]
          product.price = row["price"].to_s.gsub(",","").to_f
          product.cost_price = row["cost_price"].to_s.gsub(",","").to_f
          product.count_on_hand = row["count_on_hand"]
          product.available_on = Time.now
          product.tax_category_id = tax_category_id
          product.shipping_category_id = shipping_category_id
          product.meta_description = row["meta_description"].force_encoding("utf-8")
        #end
        product.save!

        # creating options and values
        options = row['options'].split(',').map{|el| el.strip}
        option_names = row['option_names'].split(',').map{|el| el.strip}
        variant_values = []
        puts "Options #{options.size}"
        puts "Option names #{option_names.size}"
        next unless options.size.eql?(option_names.size)

        option_names.each_with_index do |option_name, index|
            option = OptionType.find_by_name option_name
            unless option
              option = OptionType.new(:name => option_name, :presentation=>option_name)
              option.save!
            end

            if option_value = option.option_values.select{|option_value| option_value.name.eql?(options[index].force_encoding("utf-8"))}.first
              variant_values << option_value
            else
              option_value = option.option_values.create(:name => options[index], :presentation => options[index])
              option_value.save!
              variant_values << option_value
            end
            unless product.option_type_ids.include?(option.id)
              if (option.name.include?('type') and option.name.include?('gem'))
                product.option_types += [option]
              end
            end
        end
        product.save!

        #creating variants
        if variant = product.variants.select{|variant| variant.barcode.eql?(row["barcode"].force_encoding("utf-8"))}.first
          variant
        else
          variant = product.variants.create(:barcode => row["barcode"].force_encoding("utf-8"))
          variant.barcode = row["barcode"].force_encoding("utf-8")
        end
        variant.sku = row["sku"].force_encoding("utf-8")
        variant.price = row["price"].to_s.gsub(",","").to_f
        variant.cost_price = row["cost_price"].to_s.gsub(",","").to_f
        # check original file for this data
        variant.special_price = row["special_price"].blank? ? nil : row["special_price"].to_s.gsub(",","").to_f
        variant.count_on_hand = row["count_on_hand"]
        variant.option_values = variant_values
        p "Variant not saved" unless variant.save!
        product.save!

        #adding taxons to product
        row['taxonomy'] = row['taxonomy'].force_encoding("utf-8")
        next unless row['taxons']
        row['taxons']   = row['taxons'].force_encoding("utf-8")
        if taxonomy = Taxonomy.find_by_name(row['taxonomy'])
	        taxonomy = taxonomy.first if taxonomy.is_a?(Array)
        else
	        taxonomy = Taxonomy.new(:name=>row['taxonomy'])
	        taxonomy.save!
        end
        row['taxons'].split(',').each do |taxon|
            #checking
            o_taxon = Taxon.where(:name => taxon.strip, :taxonomy_id => taxonomy.id)#, :parent_id => taxonomy.taxons.first.id)
            o_taxon = o_taxon.first
            unless o_taxon
              o_taxon = Taxon.new(:name=>taxon.strip, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.taxons.first.id)
              o_taxon.save!
            end
            if o_taxon
              o_taxon.parent_id = taxonomy.taxons.first.id
              o_taxon.save
            end
            product.taxons += [o_taxon] unless product.taxon_ids.include?(o_taxon.id)
        end

        #adding the pictures
        begin
          img_dir=File.join(upload_path, 'photos',product.sku.gsub(/ /,""))
          if Dir.exists?(img_dir) and !dir_images_skus_done.include?(sku)
            pics = Dir.open(img_dir).map{|d| d} - ['.','..','Thumbs.db']
            pics.each do |pic|
              file = File.open(File.join(img_dir,pic),'rb')
              product_image = Image.new({:attachment => file,
                                  :viewable => product,
                                  :position => product.images.length
                                  })
              product.images << product_image if product_image.save
              product.save!
              dir_images_skus_done << sku
            end
          end
        rescue
          puts "error at uploading pictures"
        end
        #translations
        I18n.locale = :ru
        product.name = row["name"].force_encoding("utf-8")
        product.description = row["description"].force_encoding("utf-8")
        product.save!

        puts "Importing of product #{product.name} with sku '#{product.sku}' was finished"
    end
  end
end