# add custom rake tasks here
namespace :products do
  desc "Exports the products"
  task :export => :environment do
    csv = "\"id\";\"sku\";\"name\";\"description\";\"count_on_hand\";\"option_names\";\"options\";\"price\";\"cost_price\";\"barcode\";\n"
    Product.all.each do |product|
      product.variants.each do |variant|
	    csv << "\"#{product.id}\";\"#{variant.sku}\";\"#{product.name}\";\"#{product.description}\";\"#{product.count_on_hand}\";"
	    csv << "\"" + variant.option_values.map{|ov| ov.option_type.name}.join(",") + "\";"
	    csv << "\"" + variant.option_values.map(&:name).join(",") + "\";"
	    csv << "\"#{variant.price}\";\"#{variant.cost_price}\";\"#{variant.barcode}\";"
	    csv << "\n"
      end

      if product.variants.size < 1
        csv << "\"#{product.id}\";\"#{product.sku}\";\"#{product.name}\";\"#{product.description}\";\"#{product.count_on_hand}\";"
	      csv << "\"\";\"\";" # options
	      csv << "\"#{product.price}\";\"#{product.cost_price}\";;"
	      csv << "\n"
      end
    end
    
    # write data into the file
    file = File.open(File.join(Rails.root,"public","export.csv"), "w")
    file.syswrite(csv)
    file.close
  end

  desc "Refresh pictures for products"
  task :refresh_pictures => :environment do

   upload_path =  File.join(Rails.root, 'public','upload')
   puts "upload path #{upload_path}"
   Product.find(:all).each do |product|
     begin
       sku = product.sku.gsub(/ /,"")
       img_dir=File.join(upload_path, 'photos',sku)
       if Dir.exists?(img_dir)
         puts "image dir #{img_dir}"
         pics = Dir.open(img_dir).map{|d| d} - ['.','..','Thumbs.db']
         pics.each do |pic|
           puts "pic #{pic}"
           file = File.open(File.join(img_dir,pic),'rb')
           product_image = Image.new({:attachment => file,
                               :viewable => product,
                               :position => product.images.length
                               })
           product.images << product_image if product_image.save
           product.save!
         end
       end
     rescue
       puts :text => "Error while uploading products #{$!}"
     end
   end
  end

  desc "set russian value for options"
  task :rename_options => :environment do
    I18n.locale =:en
    engl = OptionValue.all.map{|v| [v.id, v.presentation]}
    I18n.locale =:ru
    engl.each{|e| OptionValue.find(e.first).update_attributes!({:presentation=>e.last}) }
  end

  desc "Imports the products from file public/import.csv"
  task :import => :environment do
    puts 'DEPRICATED'
      I18n.locale = :en
      # TODO: put default values
      p "TODO: put default values"
      p "TODO: uncomment taxon's search"
      tax_category_id = nil
      shipping_category_id = nil
      dir_images_skus_done = []
      upload_path =  File.join(Rails.root, 'public','upload')

      require 'csv'
      CSV.read(File.join(upload_path, 'import.csv'), {:headers => :first_row, :col_sep => ";"}).each do |row|
        begin
          I18n.locale = :en
          if row["id"]
            product = Product.find(row["id"])
          else
            product = Product.find_by_sku(row["sku"])
            if product.nil?
              product = Product.new
            end
          end

          p row["name"].force_encoding("utf-8")
          product.name = row["name"].force_encoding("utf-8")
          product.description = row["description"].blank? ? nil : row["description"].force_encoding("utf-8")
          product.sku = row["sku"].blank? ? nil : row["sku"].force_encoding("utf-8")


          #if no variant yet, we should set master price to product
          #unless variant
            p row["price"]
            product.price = row["price"].to_s.gsub(",","").to_f
            product.cost_price = row["cost_price"].to_s.gsub(",","").to_f
            product.count_on_hand = row["count_on_hand"]
            product.available_on = Time.now
            product.tax_category_id = tax_category_id
            product.shipping_category_id = shipping_category_id
            product.meta_description = row["meta_description"].blank? ? nil :row["meta_description"].force_encoding("utf-8")
          #end
          product.save!

          # creating options and values
          options = row['options'].split(',').map{|el| el.strip}
          option_names = row['option_names'].split(',').map{|el| el.strip}
          variant_values = []
          puts "Options #{options.size}"
          puts "Option names #{option_names.size}"
          next unless options.size.eql?(option_names.size)

          option_names.each_with_index do |option_name, index|
              option = OptionType.find_by_name option_name
              unless option
                option = OptionType.new(:name => option_name, :presentation=>option_name)
                option.save!
              end

              if option_value = option.option_values.select{|option_value| option_value.name.eql?(options[index].force_encoding("utf-8"))}.first
                variant_values << option_value
              else
                option_value = option.option_values.create(:name => options[index], :presentation => options[index])
                option_value.save!
                variant_values << option_value
              end
              unless product.option_type_ids.include?(option.id)
                if (option.name.include?('type') and option.name.include?('gem'))
                  product.option_types += [option]
              end
            end
          end
          product.save!

          #creating variants
          if variant = product.variants.select{|variant| variant.barcode.eql?(row["barcode"].force_encoding("utf-8"))}.first
            variant
          else
            variant = product.variants.create(:barcode => row["barcode"].force_encoding("utf-8"))
            variant.barcode = row["barcode"].force_encoding("utf-8")
          end
          variant.sku = row["sku"].force_encoding("utf-8")
          variant.price = row["price"].to_s.gsub(",","").to_f
          variant.cost_price = row["cost_price"].to_s.gsub(",","").to_f
          # check original file for this data
          variant.special_price = row["special_price"].blank? ? nil : row["special_price"].to_s.gsub(",","").to_f
          variant.count_on_hand = row["count_on_hand"]
          variant.option_values = variant_values
          p "Variant not saved" unless variant.save!
          product.save!

          #adding taxons to product
          row['taxonomy'] = row['taxonomy'].force_encoding("utf-8")
          next unless row['taxons']
          row['taxons']   = row['taxons'].force_encoding("utf-8")
          if taxonomy = Taxonomy.find_by_name(row['taxonomy'])
            taxonomy = taxonomy.first if taxonomy.is_a?(Array)
          else
            taxonomy = Taxonomy.new(:name=>row['taxonomy'])
            taxonomy.save!
          end
          row['taxons'].split(',').each do |taxon|
              #checking
              o_taxon = Taxon.where(:name => taxon.strip, :taxonomy_id => taxonomy.id)#, :parent_id => taxonomy.taxons.first.id)
              o_taxon = o_taxon.first
              unless o_taxon
                o_taxon = Taxon.new(:name=>taxon.strip, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.taxons.first.id)
                o_taxon.save!
              end
              if o_taxon
                o_taxon.parent_id = taxonomy.taxons.first.id
                o_taxon.save
              end
              product.taxons += [o_taxon] unless product.taxon_ids.include?(o_taxon.id)
          end

          #adding the pictures
          sku = product.sku.gsub(/ /,"")
          product_pic_dir = File.join(sku, product.barcode)
          img_dir=File.join(upload_path, 'photos', product_pic_dir)
          if Dir.exists?(img_dir) and !dir_images_skus_done.include?(product_pic_dir)
            pics = Dir.open(img_dir).map{|d| d} - ['.','..','Thumbs.db']
            pics.each do |pic|
              begin
                file = File.open(File.join(img_dir,pic),'rb')
                product_image = Image.new({:attachment => file,
                                    :viewable => product,
                                    :position => product.images.length
                                    })
                product.images << product_image if product_image.save
                product.save!
                dir_images_skus_done << sku
              rescue
                puts :text => "Error while setting pictures #{$!}"
              end
            end
          end

          #translations
          I18n.locale = :ru
          product.name = row["name"].force_encoding("utf-8")
          product.description = row["description"].blank? ? nil : row["description"].force_encoding("utf-8")
          product.save!

          puts "Importing of product #{product.name} with sku '#{product.sku}' was finished"
        rescue
          puts :text => "Error while uploading products #{$!}"
        end
    end
  end

  desc "Imports the products from file public/import.csv"
  task :new_import => :environment do
    require 'csv'
    I18n.locale = :en
    option_type = OptionType.find_by_name('product-size')
    option_type = OptionType.new({name: 'product-size'})  unless option_type
    option_type.save!

    upload_path =  Rails.root.join('public','upload')
    dir_images_skus_done = []
    CSV.open( upload_path.join('import.csv'),'r:UTF-8',{:headers =>true,:quote_char=>'"',:skip_blanks=>true} ).each do |row|
      break unless row
      begin
      I18n.locale = :en
      row['barcode'] = row['id'] if row['barcode'].blank?
      variant = Variant.where(:barcode => row['barcode']).first
      if variant
        product = variant.product
        product = Product.new unless product
      else
        product = Product.new
      end

      [:en, :ru].each do |locale|
        I18n.locale = locale
        #product.barcode = row['barcode']
        product.sku           = row['sku']
        product.name          = row['name']
        product.description   = row['description']
        product.meta_keywords = row['meta_title']
        product.available_on  = Time.now
        product.meta_description = row['meta_description']
        product.count_on_hand = row['count_on_hand'].to_i unless row['count_on_hand'].blank?
        product.price         = row['price'].to_f unless row['price'].blank?
        product.cost_price    = row['cost_price'].to_f unless row['cost_price'].blank?
        product.special_price = row['special_price'].to_f unless row['special_price'].blank?
        p product.errors unless product.save
      end
      master = product.master
      master.barcode       = row['barcode']
      master.sku           = row['sku']
      master.count_on_hand = row['count_on_hand'].to_i unless row['count_on_hand'].blank?
      master.price         = row['price'].to_f unless row['price'].blank?
      master.cost_price    = row['cost_price'].to_f unless row['cost_price'].blank?
      master.special_price = row['special_price'].to_f unless row['special_price'].blank?
      master.special_price = row['discounted'].to_f unless row['discounted'].blank?
      master.save!
      p product.id
      p product.sku

      I18n.locale = :en
      #adding taxons to product
      next if row['taxons'].blank?

      I18n.locale=:en
      taxonomy = Taxonomy.find_by_name(row['taxonomy'])
      unless taxonomy
        taxonomy = Taxonomy.new(:name=>row['taxonomy'])
        taxonomy.save!
      end
      taxonomy.save!
      row['taxons'].split(',').each do |taxon|
          #checking
          #o_taxon = Taxon.where(:name => taxon.strip, :taxonomy_id => taxonomy.id)
	  o_taxon = Taxon.find_all_by_name_and_taxonomy_id(taxon.strip, taxonomy.id)
          o_taxon = o_taxon.first
          unless o_taxon
            o_taxon = Taxon.new(:name=>taxon.strip, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.taxons.first.id) #,
            o_taxon.save!
          end
          if o_taxon
            #o_taxon.parent_id = taxonomy.taxons.first.id
            o_taxon.save!
          end
          product.taxons += [o_taxon] unless product.taxon_ids.include?(o_taxon.id)
          product.save!
      end
      #I18n.locale=:ru

      #setting properties
      i=0
      property_values = row['options'].split(',')
      
      product_properties = row["option_names"].split(',').map do |name|
        property = Property.find_by_name(name)
        unless property
          property = Property.new({name: name, presentation: name})
          property.save!
        end

        product_property = ProductProperty.find_by_property_id_and_value_and_product_id(property.id, property_values[i], product.id)
        unless product_property
          product_property = ProductProperty.new({property_id: property.id, value: property_values[i], product_id: product.id} )
          product_property.save!
        end
        i += 1
        product_property
      end
      product.product_properties = product_properties
      product.save!
      


      unless row['product-size'].blank?
          option_values = row['product-size'].split(',').map do |size|
            size = size.to_f
            option_value = OptionValue.find_by_option_type_id_and_name(option_type.id, size)
            unless option_value
              op_test = {option_type_id: option_type.id, name: size, presentation: size}
              p op_test
              option_value = OptionValue.new(op_test)
            end
            option_value.save!
            option_value
          end
          product.option_types += [option_type] unless product.option_type_ids.include?(option_type.id)
          product.save!


          option_values.each do |option_value|
            next if product.variants.map{|v| v.option_values}.flatten.map(&:id).include?(option_value.id) if product.variants

            variant = Variant.new({:product_id => product.id,:barcode => row['barcode']})
            variant.barcode = row['barcode']
            variant.sku = row['sku']
            variant.name = row['name']
            variant.description = row['description']
            variant.meta_keywords = row['meta_title']
            variant.available_on = Time.now
            variant.meta_description = row['meta_description']
            variant.count_on_hand = row['count_on_hand'].to_i unless row['count_on_hand'].blank?
            variant.price = row['price'].to_f unless row['price'].blank?
            variant.cost_price = row['cost_price'].to_f unless row['cost_price'].blank?
            variant.special_price = row['special_price'].to_f unless row['special_price'].blank?
	    variant.special_price = row['discounted'].to_f unless row['discounted'].blank?
            variant.option_values += [option_value]
            variant.save!
            puts "Variant id #{variant.id}"
            product.variants += [variant]
            product.save!
          end
          product.save!
          variant = product.variants.first
          variant.is_master = true unless product.master
          variant.save
      end

      #adding the pictures
      sku = product.sku.gsub(/ /,"")
      product_pic_dir = File.join(sku, master.barcode)
      img_dir=File.join(upload_path, 'photos', product_pic_dir)
	unless File.exists?(img_dir)
	  p "File or directory  #{img_dir} doesn't exist"
	  next
	end
        pics = Dir.open(img_dir).map{|d| d} - ['.','..','Thumbs.db']
        pics.each do |pic|
          begin
	    unless File.exists?(File.join(img_dir,pic))
	      p "File or directory  #{File.join(img_dir,pic)} doesn't exist"
	      next
	    end
            file = File.open(File.join(img_dir,pic),'rb')
            product_image = Image.new({:attachment => file,
                                :viewable => product,
                                :position => product.images.length
                                })
            if product_image.save
	      product.images += [product_image]
	      product.images=Hash[*product.images.map {|obj| [obj.attachment_file_name, obj]}.flatten].values
	    end
            product.save!
          rescue
            puts :text => "Error while setting pictures #{$!}"
          end
        end
    rescue Exception=>e
    end
    end
  end
  
  task :clean_up => :environment do
    Product.delete_all
    Variant.delete_all
    Order.delete_all
    LineItem.delete_all
    ProductProperty.delete_all
    Wishlist.delete_all
    WishedProduct.delete_all
  end

end

namespace :orders do
  desc 'delete wrong orders'
  task :remove_empty => :environment do
    Order.where(:item_total => 0, :total=>0, :adjustment_total=>0,:payment_total=>0).delete_all
  end
end