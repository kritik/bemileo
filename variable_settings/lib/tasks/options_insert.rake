namespace :options do
  desc "Insertiong option translations"
  task :import_translations => :environment do
    require 'csv'
    
    upload_path =  Rails.root.join( 'public','upload')
    CSV.read(File.join(upload_path, 'option_type_translations.csv')).each do |row|
      option_type = OptionType.find_by_name(row[0])
      next unless option_type
      
      I18n.locale = :ru
      # 3rd column should contain Russian name
      option_type.presentation = row[2] || row[1]
      p option_type.errors unless option_type.save
    end
  end
  
  
end