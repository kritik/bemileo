Rails.application.routes.draw do
  # Add your extension routes here
  namespace :admin do
    match 'productsloader/load_products' => 'productsloader#load_products'
    match 'productsloader/upload' => 'productsloader#upload'
    resources :custom_settings
  end
end