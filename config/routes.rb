Jshop::Application.routes.draw do
    match '/onlinedengi/:gateway_id/:order_id' => 'gateway/onlinedengi#show', :as => :onlinedengi
    match '/onlinedengi/result'  => 'gateway/onlinedengi#result',  :as => :onlinedengi_result
    match '/onlinedengi/success' => 'gateway/onlinedengi#success', :as => :onlinedengi_success
    match '/onlinedengi/fail'   => 'gateway/onlinedengi#fail',   :as => :onlinedengi_fail
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  resources :products do
    member do
      get :rate
      post :rate
      post :create_review
    end
  end

  #match '/products', :controller=>'products', :action=>'index'
  match '/product/:id/rate/:stars' => "products#rate"
  
  #blog entries
  constraints :subdomain => 'blog' do
    scope :module => 'blog', :as => 'blog', :subdomain => 'blog' do
      resources :news, :controller => :blog_entries
    end
    match '/:year/:month/:day/:slug', :to => 'blog_entries#show', :as => :entry_permalink

    match '/:year(/:month)(/:day)', :to => 'blog_entries#archive', :as => :news_archive, :constraints => {:year => /(19|20)\d{2}/, :month => /[01]?\d/, :day => /[0-3]?\d/}

    match '/tag/:tag', :to => 'blog_entries#tag', :as => :tag
#     match '/' => 'static_content#show', :defaults =>{:path=>'/magazine'}
    match '/' => 'blog_entries#index'
  end
end
