# encoding: utf-8

=begin
module OmniAuth
  module Strategies
    # Authentication strategy for connecting with APIs constructed using
    # the [OAuth 2.0 Specification](http://tools.ietf.org/html/draft-ietf-oauth-v2-10).
    # You must generally register your application with the provider and
    # utilize an application id and secret in order to authenticate using
    # OAuth 2.0.
    class OAuth2
      def client22
        client_options = options[:client_options] || {}
        client_options[:ssl] = client_options[:ssl] || {}
        client_options[:ssl][:ca_path] = client_options[:ssl][:ca_path] || '/etc/ssl/certs'
        ::OAuth2::Client.new(client_id, client_secret, client_options.merge(client_options))
      end
    end
  end
end
module Faraday
  class Adapter
    class NetHttp < Faraday::Adapter
      def call32(env)
        super
        url = env[:url]
        req = env[:request]

        http = net_http_class(env).new(url.host, url.inferred_port)
        
        p "fddddddddddd"
        p "fddddddddddd"
        p "fddddddddddd"
        p env[:ssl]
        if http.use_ssl = (url.scheme == 'https' && env[:ssl])
          http.use_ssl = true
          ssl = env[:ssl]
          p "GGGGGGGGGGGGGGg"
          p "GGGGGGGGGGGGGGg"
          p "GGGGGGGGGGGGGGg"
          p "GGGGGGGGGGGGGGg"
          p ssl
          http.verify_mode = ssl[:verify_mode] || ssl.fetch(:verify, true) ?
                               OpenSSL::SSL::VERIFY_PEER : OpenSSL::SSL::VERIFY_NONE
          http.cert        = ssl[:client_cert] if ssl[:client_cert]
          http.key         = ssl[:client_key]  if ssl[:client_key]
          http.ca_file     = ssl[:ca_file]     if ssl[:ca_file]
          http.ca_path     = ssl[:ca_path]     if ssl[:ca_path]
          http.cert_store  = ssl[:cert_store]  if ssl[:cert_store]
        end

        http.read_timeout = http.open_timeout = req[:timeout] if req[:timeout]
        http.open_timeout = req[:open_timeout]                if req[:open_timeout]

        if :get != env[:method]
          http_request = Net::HTTPGenericRequest.new \
            env[:method].to_s.upcase,    # request method
            !!env[:body],                # is there data
            true,                        # does net/http love you, true or false?
            url.request_uri,             # request uri path
            env[:request_headers]        # request headers

          if env[:body].respond_to?(:read)
            http_request.body_stream = env[:body]
            env[:body] = nil
          end
        end

        begin
          http_response = if :get == env[:method]
            # prefer `get` to `request` because the former handles gzip (ruby 1.9)
            http.get url.request_uri, env[:request_headers]
          else
            http.request http_request, env[:body]
          end
        rescue Errno::ECONNREFUSED
          raise Error::ConnectionFailed, $!
        end

        save_response(env, http_response.code.to_i, http_response.body) do |response_headers|
          http_response.each_header do |key, value|
            response_headers[key] = value
          end
        end

        @app.call env
      end
    end
  end
end

module SpreeSocial
  OAUTH_PROVIDERS = [
    ["Bit.ly", "bitly"], ["Evernote", "evernote"], ["Facebook", "facebook"], ["Foursquare", "foursquare"],
    ["Github", "github"], ["Google", "google"] , ["Gowalla", "gowalla"], ["instagr.am", "instagram"],
    ["Instapaper", "instapaper"], ["LinkedIn", "linked_in"], ["37Signals (Basecamp, Campfire, etc)", "thirty_seven_signals"],
    ["Twitter", "twitter"], ["Vimeo", "vimeo"], ["Yahoo!", "yahoo"], ["YouTube", "you_tube"], ["Vkontakte","vkontakte"], ["Mailru", 'mailru']
  ]
end
=end

start = Time.now
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
if Spree::Config.instance
  Spree::Config.set(:default_locale => 'ru')
  Spree::Config.set(:default_country_id => '168')
  Spree::Config.set(:products_per_page => 30)
  #Spree::Config.set(:stylesheets => 'screen')

  Product.scope :with_property_value, lambda { |property, value|
    conditions = case property
    when String then ["properties.name = ?", property]
    when Property then ["properties.id = ?", property.id]
    else ["properties.id = ?", property.to_i]
    end
    conditions = ["lower(product_properties.value) LIKE ? AND #{conditions[0]}", "%#{value.downcase}%", conditions[1]]

    {
      :joins => :properties,
      :conditions => conditions
    }
  }
  
  Product.scope :in_name, lambda{|words|
    a = words.split(/[,]/).map(&:strip)
    a=a.any? ? a : ['']
    Product.like_any([:name], a)
  }
  
  
  # Product's solr search
  max  = Variant.maximum(:price).to_f
  min  = Variant.minimum(:price).to_f
  step = max/6
  ranges = []
  (min..max).step(step){|s| ranges << s }
  
  I18n.locale = :ru
  PRODUCT_PRICE_RANGES = {}
  #PRODUCT_PRICE_RANGES = {0..6000 => " Under $25", 25..50 => " $25 to $50",
  #                       50..12000 => " $50 to $100", 12000..99999999 => "$100 to $200"}
  ranges.each_with_index{|e,i|  PRODUCT_PRICE_RANGES.merge!({e..(ranges[i+1]) => " #{e.to_i} #{I18n.t(:to, :default => 'до')} #{ranges[i+1].to_i}" }) if ranges[i+1] }

  translated_properties = Property.all.map{|p| "#{p.name.strip}_property" unless p.presentation.nil? }.compact-PRODUCT_SOLR_FIELDS.map(&:to_s)   

  Product.class_eval do
    def my_properties(name)
      pp = ProductProperty.first(:joins => :property,
                   :conditions => {:product_id => self.id, :properties => {:name => name}})
      pp ? pp.value : ''
    end
  end
  
  translated_properties.each do |pp|
    pp_method_name = pp.gsub("-","_")
    method = "def #{pp_method_name}
      my_properties(\"#{pp}\")
    end"
    Product.class_eval do
      eval(method)
    end
    PRODUCT_SOLR_FIELDS << pp_method_name.to_sym
  end
  
  
  PRODUCT_SOLR_FIELDS << :sku
end
