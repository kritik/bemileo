//=========================================================================================================== Tooltips
$(document).ready(function(){
						   
	$(".tooltip_init").hover(
			function(){ //over
	 				$(this).children("div.tooltip").css({'display':'block'});
			},
    		function(){ //out
	  				$(this).children("div.tooltip").css({'display':'none'});
			  }
	);
});

//======================================================================================================== Switch Tabs Table

$(document).ready(function(){
		var $id_current = 'tab1';
		var $label_current;
		
		$("#unit_tabs ul.unit_tabs_label li:first-child a").addClass("current");
		$("#unit_tabs .unit_tabs_content div#tab1").css({'display' : 'block'});
		
		$("#unit_tabs ul.unit_tabs_label li a").click(function() { 
				$id_current = $(this).attr("rel");						
				$label_current = $(this);
				
				$("#unit_tabs ul.unit_tabs_label li a").each(function(iww) { 
   						$(this).removeClass("current");
    			});
				$label_current.addClass("current");
				
				$("#unit_tabs .unit_tabs_content div.page").each(function(idd) { 
   						$(this).css({'display' : 'none'});
    			});
				$("#unit_tabs .unit_tabs_content div#"+$id_current).css({'display' : 'block'});
		
				return false;
		});
		
});

//================================================================================================= List Filters dropdown 

$(document).ready(function(){
		$(".set_filter ul").css({'display' : 'none'});				   
		$(".set_filter ul").each(function() {  							
   				var $ul_pre =  $(this),
					ul_presize =  $ul_pre.children('li').size(); 	
				if(ul_presize>5){				
						($ul_pre.children('li')).eq(4).append("<span></span>");
						($ul_pre.children('li')).eq(4).children("span").addClass("more");
						for (var i_li = 0; i_li < ul_presize; i_li++){
    							($ul_pre.children('li')).eq(i_li).css({'display' : 'none'});
						}
				}else{
						$ul_pre.css({'display' : 'none'});
				};
    	});
		
		
		$(".set_filter h2").click(function() {
				var $ul_li =  $(this).next("ul"),
					ul_li_size =  $ul_li.children('li').size(),
					height_rem = 0; 									
				
				if(ul_li_size<6){    
						$ul_li.slideToggle(500);
				}else{									
						if($(this).hasClass("current")==true){
								$ul_li.slideToggle(200);
								$ul_li.children('li').children('span.more').css({'display' : 'block'}); 
								for (var ii = 0; ii < ul_li_size; ii++){
    									($ul_li.children('li')).eq(ii).css({'display' : 'none'});
								}
						}else{
								for (var iii = 0; iii < 5; iii++){
    								($ul_li.children('li')).eq(iii).css({'display' : 'block'});
								};
								$ul_li.slideToggle(500);
						}
				};
				$(this).toggleClass("current");		
		});
		
		
		$("span.more").click(function() {
				var $ul_more = $(this).parent("li").parent("ul");
				
				$(this).css({'display' : 'none'});
				$ul_more.children('li').show(500);;
		});
});
